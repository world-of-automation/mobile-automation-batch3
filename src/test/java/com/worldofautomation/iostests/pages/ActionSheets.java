package com.worldofautomation.iostests.pages;

import com.worldofautomation.mobileautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ActionSheets {

    @FindBy(xpath = "//XCUIElementTypeNavigationBar[@name='Action Sheets']")
    private WebElement actionSheetsTitle;

    @FindBy(xpath = "//XCUIElementTypeButton[@name='UICatalog']")
    private WebElement backBtn;


    public void validateActionSheetsPageIsDisplayed() {
        Assert.assertTrue(actionSheetsTitle.isDisplayed());
        ExtentTestManager.log(" actionsheets title is displayed");
    }

    public void clickBackButton() {
        backBtn.click();
        ExtentTestManager.log("Back button is clicked");

    }
}
