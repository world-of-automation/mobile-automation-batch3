package com.worldofautomation.iostests.pages;

import com.worldofautomation.mobileautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(xpath = "//XCUIElementTypeStaticText[@value='Action Sheets']")
    private WebElement actionSheets;

    @FindBy(xpath = "//XCUIElementTypeStaticText[@value='Activity Indicators']")
    private WebElement activityIndicator;


    // create a method which will take 2 params, 1 web elements and 1 string
    // and return true or false based on if the element's text matching with the text provided or not

    public boolean methodaaaaa(WebElement element, String data) {
        return element.getText().equalsIgnoreCase(data);
    }

    //you are given a string APPLE, reverse the string

/*

    public static void main(String[] args) {

        String name = "APPLE";
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i=0;i<name.length();i++){
            arrayList.add(name.charAt(i));
        }

        System.out.println(arrayList);

        String reversedName = "";
        for (int i=arrayList.size()-1;i>=0;i--){
            reversedName=reversedName+arrayList.get(i);
        }

        System.out.println(reversedName);
    }
*/

   /* @AndroidFindBy(xpath = "android xpath/")
    @iOSXCUITFindBy(xpath = "ios xpath")
    private MobileElement ss;


    public void validateSSisDisplayed(){
        Assert.assertTrue(ss.isDisplayed());
    }*/


    public void validateActionSheetsButtonIsDisplayed() {
        Assert.assertTrue(actionSheets.isDisplayed());
        ExtentTestManager.log("actionSheets is Displayed");
    }


    public void validateActionSheetsIsClickAble() {
        actionSheets.click();
        ExtentTestManager.log("actionSheets is clicked");
    }
}
