package com.worldofautomation.iostests.tests;

import com.worldofautomation.iostests.pages.ActionSheets;
import com.worldofautomation.iostests.pages.HomePage;
import com.worldofautomation.mobileautomation.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class HomePageTests extends TestBase {


    @Test
    public void validateUserBeingAbleToNavigateBetweenButton() {
        sleepFor(5);
        HomePage homePage = PageFactory.initElements(getAppiumDriver(), HomePage.class);
        ActionSheets actionSheets = PageFactory.initElements(getAppiumDriver(), ActionSheets.class);

        homePage.validateActionSheetsButtonIsDisplayed();
        homePage.validateActionSheetsIsClickAble();
        actionSheets.validateActionSheetsPageIsDisplayed();
        actionSheets.clickBackButton();

        sleepFor(10);
    }
}
