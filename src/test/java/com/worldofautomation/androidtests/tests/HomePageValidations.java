package com.worldofautomation.androidtests.tests;

import com.worldofautomation.androidtests.pages.HomePage;
import com.worldofautomation.mobileautomation.ExtentTestManager;
import com.worldofautomation.mobileautomation.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class HomePageValidations extends TestBase {


    @Test(enabled = false)
    public void validateUserBeingAbleToUseHeaderButtons() {
        HomePage homePage = PageFactory.initElements(getAppiumDriver(), HomePage.class);
        homePage.validateHeaderButtonsAreWorking();
    }

    @Test
    public void validateSwipe() {
        HomePage homePage = PageFactory.initElements(getAppiumDriver(), HomePage.class);
        homePage.validateHomePageIsDisplayed();
        sleepFor(10);

        functionSwipe("Up", 200, 1000);
        sleepFor(10);
        ExtentTestManager.log("Performed swipe");
    }
}
