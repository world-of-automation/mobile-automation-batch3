package com.worldofautomation.androidtests.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(xpath = "//android.widget.Button[@text='Transfer']")
    private WebElement transferBtn;

    @FindBy(xpath = "//android.widget.Button[@text='Pay A Bill']")
    private WebElement payAbillBtn;

    @FindBy(xpath = "//android.widget.Button[@text='Accounts']")
    private WebElement accountsBtn;

    @FindBy(xpath = "//android.widget.Button[@text='Deposit']")
    private WebElement depositBtn;

    @FindBy(xpath = "//android.widget.Button[@text='Send Money']")
    private WebElement sendMoneyBtn;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Go Back']")
    private WebElement backBtn;

    public void validateHeaderButtonsAreWorking() {
        transferBtn.click();
        backBtn.click();
        accountsBtn.click();
        backBtn.click();
        payAbillBtn.click();
        backBtn.click();
        sendMoneyBtn.click();
        backBtn.click();
        depositBtn.click();
    }


    public void validateHomePageIsDisplayed() {
        Assert.assertTrue(accountsBtn.isDisplayed());
    }
}
