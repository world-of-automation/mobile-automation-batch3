package com.worldofautomation.mobileautomation;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppuimLaunchiOS {

    public static void main(String[] args) throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "13.4");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, "58770b8740b634260032a934819341b388bee05c");
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        desiredCapabilities.setCapability("bundleId", "com.example.apple-samplecode.UICatalog");
        AppiumDriver appiumDriver = new IOSDriver(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
        // "http://host:port/wd/hub"

        appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        System.out.println(appiumDriver.getPageSource());


    }
}
