package com.worldofautomation.mobileautomation;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppuimLaunchAndroid {

    public static void main(String[] args) throws MalformedURLException, InterruptedException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.1");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, "emulator-5554");

        // run the tests
        desiredCapabilities.setCapability(MobileCapabilityType.APP_PACKAGE, "com.espn.score_center");// ask your dev or appium console
        desiredCapabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.espn.sportscenter.ui.EspnLaunchActivity");// ask your dev or appium console
        // to install and get the package and activity
        // desiredCapabilities.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir")+"/src/main/resources/ESPN.apk");

        AppiumDriver appiumDriver = new AndroidDriver(new URL("http://localhost:6655/wd/hub"), desiredCapabilities);
        // "http://host:port/wd/hub"

        appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        System.out.println(appiumDriver.getPageSource());

        Thread.sleep(10000);

        appiumDriver.findElement(By.id("com.espn.score_center:id/btn_sign_up_later")).click();

        appiumDriver.findElement(By.xpath("(//android.widget.FrameLayout[@resource-id='com.espn.score_center:id/animation_view'])[2]")).click();

        Thread.sleep(10000);
        appiumDriver.quit();
    }
}
